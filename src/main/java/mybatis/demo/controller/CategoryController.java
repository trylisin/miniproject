package mybatis.demo.controller;

import mybatis.demo.Repositery.Modal.Category;
import mybatis.demo.service.CategoryService.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
public class CategoryController {

    @Autowired
    CategoryService categoryServiceImp;

    @GetMapping({"/category"})
    public String category(ModelMap modelMap){
        Category category = new Category();
        category.setId(categoryServiceImp.getlastId()+1);
        modelMap.addAttribute("getCategory",categoryServiceImp.findall());
        modelMap.addAttribute("Category",category);
        return "Category/viewCategory";
    }

    @GetMapping("/addcategory")
    public String addCategory(ModelMap modelMap){
        Category category = new Category();
        category.setId(categoryServiceImp.getlastId()+1);
        modelMap.addAttribute("addcategory", category);
        return "Category/addCategory";
    }

    @PostMapping("/addcategory")
    public String add(@Valid @ModelAttribute("addcategory") Category category, BindingResult bindingResult,ModelMap modelMap){

        if(bindingResult.hasErrors()){
            System.out.println("erorr");
            modelMap.addAttribute("addcategory",category);
            return "Category/addCategory";
        }

        categoryServiceImp.add(category);
        System.out.println("success");
        return "redirect:/category";
    }

    @GetMapping("/deleteCategory/{id}")
    public String delete(@PathVariable("id") int id){
        categoryServiceImp.delete(id);
        return "redirect:/category";
    }

    @GetMapping("/updateCategory/{id}")
    public String updateArticle(ModelMap modelMap,@PathVariable("id") int id){
        modelMap.addAttribute("allCategory",categoryServiceImp.findall());
        modelMap.addAttribute("categorybyid", categoryServiceImp.findbyid(id));
        return "Category/updateCategory";
    }

    @PostMapping("/updateCategory")
    public String update(@Valid @ModelAttribute("categorybyid") Category category,BindingResult bindingResult,ModelMap modelMap){
        if(bindingResult.hasErrors()){
            System.out.println("erorr");
            modelMap.addAttribute("categorybyid",category);
            return "Category/updateCategory";
        }

        categoryServiceImp.update(category);
        System.out.println("success");
        return "redirect:/category";


    }
}
