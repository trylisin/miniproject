package mybatis.demo.controller;

import mybatis.demo.Repositery.Modal.Article;
import mybatis.demo.Repositery.Modal.ArticleFilter;
import mybatis.demo.Repositery.Modal.Category;
import mybatis.demo.service.ArticleServiceImp;
import mybatis.demo.service.CategoryService.CategoryService;
import mybatis.demo.service.CategoryServiceImp;
import mybatis.demo.service.articleservice.ArticleService;
import mybatis.demo.utility.Paging;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.UUID;

@Controller
public class ArticleController {

    @Autowired
    ArticleService articleServiceImp;

    @Autowired
    CategoryService categoryService;
    private String thumbnails;

    static int currentpage =1;

//    @GetMapping("/home")
//    public String index(){
//        return "index";
//    }

    @GetMapping({"/add"})
    public String addArticle(ModelMap modelMap){
        Article article = new Article();
        article.setId(articleServiceImp.getlastId()+1);
        article.setTitle(null);
        modelMap.addAttribute("getArticle",articleServiceImp.findall());
        modelMap.addAttribute("articles",article);
        modelMap.addAttribute("allCate",categoryService.findall());
        return "Article/index";
    }

    @PostMapping("/add")
    public String add(@Valid @ModelAttribute("articles") Article article, BindingResult bindingResult, @RequestParam("file") MultipartFile file,ModelMap modelMap){
        if(bindingResult.hasErrors()){
            System.out.println("erorr");
            modelMap.addAttribute("articles",article);
            modelMap.addAttribute("allCate", categoryService.findall());
            return "Article/index";
        }
            configFileName(article, file);
            articleServiceImp.add(article);
            System.out.println("success");
            return "redirect:/view";
    }
    @GetMapping("/info/{id}")
    public String infomation(ModelMap modelMap,@PathVariable("id") int id){
        modelMap.addAttribute("ViewArticle", articleServiceImp.findbyid(id));
        return "Article/infoArticle";
    }
    @GetMapping("/update/{id}")
    public String updateArticle(ModelMap modelMap,@PathVariable("id") int id){
        thumbnails = articleServiceImp.findbyid(id).getThumbnail();
        modelMap.addAttribute("allCate",categoryService.findall());
        modelMap.addAttribute("article", articleServiceImp.findbyid(id));
        return "Article/update";
    }

    @PostMapping("/update")
    public String update(@ModelAttribute Article article, MultipartFile file){
        configFileName(article, file);
        articleServiceImp.update(article);
        return "redirect:/view";
    }


    @GetMapping({"/view","/"})
    public String showViewer(ArticleFilter filter, Paging paging,ModelMap modelMap){
        Article article = new Article();
        Category category = new Category();
        modelMap.addAttribute("getArticle", articleServiceImp.findAllFilter(filter,paging));
        modelMap.addAttribute("filter", filter);
        modelMap.addAttribute("articles",article);
        modelMap.addAttribute("allCate",categoryService.findall());
        return "Article/view";
    }
    int totalPage() {
        return (int)(Math.ceil((double)articleServiceImp.findall().size() / 5));
    }
    @GetMapping("/delete/{id}")
    public String delete(@PathVariable("id") int id){
        articleServiceImp.delete(id);
        return "redirect:/view";
    }

    private void configFileName(Article article, MultipartFile file) {
        String fileName = UUID.randomUUID().toString();
        String extension;
        if(file != null && file.getOriginalFilename() != null) {
            if(!file.getOriginalFilename().isEmpty()) {
                extension = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf('.'));
                fileName += extension;
                try {
                    Files.copy(file.getInputStream(), Paths.get("src/main/resources/thumbnail/" + fileName));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                System.out.println(fileName);
                article.setThumbnail(fileName);
                return;
            }
        }
        if(thumbnails != null) {
            article.setThumbnail(thumbnails);
            thumbnails = null;
        }
        else
            article.setThumbnail("default.jpg");
    }

    @GetMapping("/frag1")
    public String getFragment() {
        return "Article/navbar :: navbar";
    }

    @GetMapping("/frag2")
    public String getFragment2() {
        return "Article/navbar :: navbar2";
    }

    @GetMapping("/search")
    public String SearchById(@RequestParam String title,ArticleFilter filter, Paging paging, ModelMap modelMap) {
        modelMap.addAttribute("getArticle", articleServiceImp.findAllFilter(filter,paging));
        if (articleServiceImp.searchByTitle(title).size()==0){
            return "redirect:/view";
        }
        modelMap.addAttribute("getArticle", articleServiceImp.searchByTitle(title));
        modelMap.addAttribute("categories", categoryService.findall());

        return "Article/view";
        }

    @GetMapping("/filter")
    public String filterByCategory(@RequestParam Integer id, ModelMap modelMap) {

        modelMap.addAttribute("getArticle", articleServiceImp.searchByCategory(id));
        modelMap.addAttribute("categories", categoryService.findall());
        return "Article/view" + "";
    }

}
