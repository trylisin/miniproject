package mybatis.demo.Repositery.CategoryRepository;


import mybatis.demo.Repositery.Modal.Category;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepository {

    @Insert("INSERT into tb_category (title) values (#{title})")
    void add(Category category);

    @Update("update tb_category set title = #{title} where id = #{id}")
    void update(Category category);

    @Delete("delete from tb_category where id = #{id}")
    void delete(int id);


    @Select("Select * from tb_category")
    List<Category> findall();

    @Select("Select * from tb_category where id = #{id}")
    Category findbyid(int id);

    @Select("Select * from tb_category")
    List<Category> getByPage(int start, int end);

    @Select("Select id from tb_category order by id desc limit 1")
    int getlastId();
}
