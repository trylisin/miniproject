package mybatis.demo.Repositery.provider;

import mybatis.demo.Repositery.Modal.ArticleFilter;
import mybatis.demo.utility.Paging;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;

public class ArticleProvider {
    public String findAllFilter(@Param("filter") ArticleFilter articleFilter, @Param("paging") Paging paging){
        return new SQL(){{
            SELECT("a.*,c.title catename");
            FROM("tb_article a");
            INNER_JOIN("tb_category c on a.category_id=c.id");
            if(articleFilter.getTitle()!=null)
                WHERE("a.title ILIKE '%'||#{filter.title}||'%'");
            if(articleFilter.getCate_id()!=null)
                WHERE("a.category_id = #{filter.cate_id}");
            ORDER_BY("a.id ASC limit #{paging.limit} offset #{paging.offset}");
        }}.toString();
    }


    public String countAllArticles(ArticleFilter articleFilter){
        return new SQL(){{
            SELECT("COUNT(*)");
            FROM("tb_article");
            if(articleFilter.getTitle()!=null)
                WHERE("title ILIKE '%'||#{title}||'%'");
            if(articleFilter.getCate_id()!=null)
                WHERE("category_id = #{cate_id}");
        }}.toString();
    }
}
