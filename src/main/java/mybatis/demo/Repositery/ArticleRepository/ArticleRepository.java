package mybatis.demo.Repositery.ArticleRepository;

import mybatis.demo.Repositery.Modal.Article;
import mybatis.demo.Repositery.Modal.ArticleFilter;
import mybatis.demo.Repositery.provider.ArticleProvider;
import mybatis.demo.utility.Paging;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;


@Repository
public interface ArticleRepository {

    @Insert("INSERT into tb_article (title,author,category_id,description,thumbnail) values (#{title},#{author},#{category.id},#{description},#{thumbnail})")
    void add(Article article);

    @Update("update tb_article set title = #{title},author = #{author},category_id = #{category.id},description = #{description},thumbnail = #{thumbnail} where id = #{id}")
    void update(Article article);

    @Delete("delete from tb_article where id = #{id}")
    void delete(int id);


    @Select("select a.*,c.title catename from tb_article a inner join tb_category c on a.category_id=c.id order by a.id asc")
    @Results({
            @Result(property = "category.id",column = "category_id"),
            @Result(property = "category.title",column = "catename")
    })
    List<Article> findall();

    @Select("select a.*,c.title catename from tb_article a inner join tb_category c on a.category_id=c.id where a.id=#{id}")
    @Results({
            @Result(property = "category.id",column = "category_id"),
            @Result(property = "category.title",column = "catename")
    })
    Article findbyid(int id);

    @Select("Select * from tb_article")
    List<Article> getByPage(int start, int end);

    @Select("Select id from tb_article order by id desc limit 1")
    int getlastId();


    @SelectProvider(method = "findAllFilter",type = ArticleProvider.class)
    @Results({
            @Result(property = "category.id",column = "cate_id"),
            @Result(property = "category.title",column = "catename")
    })
    List<Article> findAllFilter(@Param("filter") ArticleFilter articleFilter, @Param("paging") Paging paging);

    @SelectProvider(method = "countAllArticles",type = ArticleProvider.class)
    public Integer countAllArticles(ArticleFilter articleFilter);


    @Select("SELECT * FROM tb_article as article join tb_category as cate on article.category_id=cate.id where article.title ilike '%'||#{title}||'%'")
    @Results({
            @Result(property = "category.id",column = "cate_id"),
            @Result(property = "category.title",column = "catename")
    })

    ArrayList<Article> searchByTitle(String title);

    @Select("SELECT * FROM tb_article as article join tb_category as cate on article.category_id=cate.category_id where article.category_id=#{cateId}")
    @Results({
            @Result(property = "category.id",column = "cate_id"),
            @Result(property = "category.title",column = "catename")
    })
    ArrayList<Article> searchByCategoryId(Integer cateId);
}
