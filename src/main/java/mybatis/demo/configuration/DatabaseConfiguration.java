package mybatis.demo.configuration;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

import javax.sql.DataSource;

@Configuration
@MapperScan("com.hrd.demo.demo.Repositery.ArticleRepository")
@MapperScan("com.hrd.demo.demo.Repositery.CategoryRepository")
public class DatabaseConfiguration {

    @Bean
    @Profile("memory")
    public DataSource memoryDB(){
        EmbeddedDatabaseBuilder embeddedDatabaseBuilder = new EmbeddedDatabaseBuilder();
        embeddedDatabaseBuilder.setType(EmbeddedDatabaseType.H2);
        embeddedDatabaseBuilder.addScripts("classpath:/static/sql/table.sql","classpath:/static/sql/data.sql");
        return embeddedDatabaseBuilder.build();
    }
}
