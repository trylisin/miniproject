package mybatis.demo.service.articleservice;


import mybatis.demo.Repositery.Modal.Article;
import mybatis.demo.Repositery.Modal.ArticleFilter;
import mybatis.demo.utility.Paging;
import org.apache.ibatis.annotations.Param;

import java.util.ArrayList;
import java.util.List;

public interface ArticleService {
    void add(Article article);
    void update(Article article);
    void delete(int id);
    List<Article> findall();
    Article findbyid(int id);
    int getlastId();
    ArrayList<Article> searchByTitle(String title);
    ArrayList<Article> searchByCategory(Integer id);
    List<Article> getByPage(int page);
    List<Article> findAllFilter(@Param("filter") ArticleFilter articleFilter, @Param("paging") Paging paging);
}
