package mybatis.demo.service.CategoryService;
import mybatis.demo.Repositery.Modal.Category;

import java.util.List;

public interface CategoryService {

    void add(Category category);
    void update(Category category);
    void delete(int id);
    List<Category> findall();
    Category findbyid(int id);
    int getlastId();
    List<Category> getByPage(int page);
}
