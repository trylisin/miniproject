package mybatis.demo.service;

import mybatis.demo.Repositery.ArticleRepository.ArticleRepository;
import mybatis.demo.Repositery.Modal.Article;
import mybatis.demo.Repositery.Modal.ArticleFilter;
import mybatis.demo.service.articleservice.ArticleService;
import mybatis.demo.utility.Paging;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;
@Service
public class ArticleServiceImp implements ArticleService {
    @Autowired
    ArticleRepository articleRepository;


    @Override
    public ArrayList<Article> searchByTitle(String title) {

        return articleRepository.searchByTitle(title);
    }

    @Override
    public ArrayList<Article> searchByCategory(Integer id) {

        return articleRepository.searchByCategoryId(id);
    }

    @Override
    public void add(Article article) {
        articleRepository.add(article);
    }

    @Override
    public void update(Article article) {
        articleRepository.update(article);
    }

    @Override
    public void delete(int id) {
        articleRepository.delete(id);
    }

    @Override
    public List<Article> findall() {
        return articleRepository.findall();
    }

    @Override
    public List<Article> getByPage(int page) {
        int start = (page - 1) * 5;
        int end = start + 5;
        if(end >= articleRepository.findall().size()) {
            end = articleRepository.findall().size();
        }
        return articleRepository.getByPage(start, end);
    }

    @Override
    public Article findbyid(int id)
    {
        return articleRepository.findbyid(id);
    }

    @Override
    public int getlastId() {
        return articleRepository.getlastId();
    }
    @Override
    public List<Article> findAllFilter(ArticleFilter articleFilter, Paging paging) {
        paging.setTotalCount(articleRepository.countAllArticles(articleFilter));
        return articleRepository.findAllFilter(articleFilter,paging);
    }
}
