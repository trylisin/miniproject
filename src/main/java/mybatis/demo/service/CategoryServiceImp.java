package mybatis.demo.service;


import mybatis.demo.Repositery.CategoryRepository.CategoryRepository;
import mybatis.demo.Repositery.Modal.Category;
import mybatis.demo.service.CategoryService.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImp implements CategoryService {

    @Autowired
    CategoryRepository categoryRepository;
    @Override
    public void add(Category category) {
        categoryRepository.add(category);
    }

    @Override
    public void update(Category category) {
        categoryRepository.update(category);
    }

    @Override
    public void delete(int id) {
        categoryRepository.delete(id);
    }

    @Override
    public List<Category> findall() {
        return categoryRepository.findall();
    }
    @Override
    public Category findbyid(int id) {
        return categoryRepository.findbyid(id);
    }

    @Override
    public int getlastId() {
        return categoryRepository.getlastId();
    }

    @Override
    public List<Category> getByPage(int page) {
        int start = (page - 1) * 5;
        int end = start + 5;
        if(end >= categoryRepository.findall().size()) {
            end = categoryRepository.findall().size();
        }
        return categoryRepository.getByPage(start, end);
    }
}
